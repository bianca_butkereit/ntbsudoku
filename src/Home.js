//import React, { Component , useState }  from 'react';
//import ReactDOM from "react-dom";
//import { Link } from 'react-router-dom';
//import fire from './config/Fire';
//import Modal from 'react-bootstrap/Modal';
//import Button from 'react-bootstrap/Button';
//import { BrowserRouter as Router, Route} from 'react-router-dom';
//import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
//import { homedir, userInfo } from "os";
//import './css/App.css';
/*
const [show, setShow] = useState(false);
const handleClose = () => setShow(false);
const handleShow = () => setShow(true);

class Home extends Component {
  
    render() {
     
        return (
            <body>
                <div class="app">
    <div class="print-main page">
        <Button id="pdf-download" onClick="download()">
            <i class="fa  fa-file-pdf-o"></i>
        </Button>




        <Button id="settings" type="button" class="btn btn-info" onClick={handleShow}>
            <i class="fa fa-cog"></i>
        </Button>
    </div>
    <Modal show={show} onHide={handleClose} animation={false}>
        <Modal.Header closeButton>
            <Modal.Title>Sudoku Setup</Modal.Title>
        </Modal.Header>
        <Modal.Body>
            <div class="row">
                <div class="col-xs-12">
                    <div class="tab-content">
                        <div id="general-settings">
                            <fieldset>
                                <form class="form-horizontal" >
                                    <div class="form-group">
                                        <label for="nod" class="col col-sm-5">Number of Difficulties</label>
                                        <div class="col col-sm-7">
                                            <select class="form-control" id="nod">
                                                <option value="1">One</option>
                                                <option value="2">Two</option>
                                                <option value="3">Three</option>
                                                <option value="4">Four</option>
                                                <option value="5">Five</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="print-dif" class="col col-sm-5">Difficulties to Print</label>
                                        <div class="col col-sm-7">
                                            <input type="checkbox" class="diff_level" id="diff_level_0"
                                                value="0" />&nbsp;ENKLESTE<br />
                                            <input type="checkbox" class="diff_level" id="diff_level_1"
                                                value="0" />&nbsp;LETTI<br />
                                            <input type="checkbox" class="diff_level" id="diff_level_2"
                                                value="0" />&nbsp;MIDDELS<br />
                                            <input type="checkbox" class="diff_level" id="diff_level_3"
                                                value="0" />&nbsp;VANSKELIG<br />
                                            <input type="checkbox" class="diff_level" id="diff_level_4"
                                                value="0" />&nbsp;NÅDELØS<br />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="font-family-boardtitle" class="col col-sm-5">Font</label>
                                        <div class="col col-sm-7">
                                            <select class="form-control" id="font-family-boardtitle">
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="font-size-boardtitle" class="col col-sm-5">Font Size</label>
                                        <div class="col col-sm-7">
                                            <div class="input-group">
                                                <input type="text" class="form-control" id="font-size-boardtitle"
                                                    placeholder="Font Size for Board Name" />
                                                <span class="input-group-addon">mm</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="color-boardtitle" class="col col-sm-5">Color</label>
                                        <div class="col col-sm-7">
                                            <input type="text" class="form-control" id="color-boardtitle" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="level-title-0" class="col col-sm-5">Easiest</label>
                                        <div class="col col-sm-7">
                                            <input type="text" class="form-control" id="level-title-0"
                                                placeholder="ENKLESTE" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="level-title-1" class="col col-sm-5">Easy</label>
                                        <div class="col col-sm-7">
                                            <input type="text" class="form-control" id="level-title-1"
                                                placeholder="LETTI" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="level-title-2" class="col col-sm-5">Medium</label>
                                        <div class="col col-sm-7">
                                            <input type="text" class="form-control" id="level-title-2"
                                                placeholder="MIDDELS" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="level-title-3" class="col col-sm-5">Hard</label>
                                        <div class="col col-sm-7">
                                            <input type="text" class="form-control" id="level-title-3"
                                                placeholder="VANSKELIG" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="level-title-4" class="col col-sm-5">Hardest</label>
                                        <div class="col col-sm-7">
                                            <input type="text" class="form-control" id="level-title-4"
                                                placeholder="NÅDELØS" />
                                        </div>
                                    </div>
                                </form>
                            </fieldset>
                        </div>
                        <div class="tab-pane" id="print-settings">
                            <fieldset>
                                <form class="form-horizontal" >
                                    <div class="form-group">
                                        <label for="sob" class="col col-sm-5">Size of Board</label>
                                        <div class="col col-sm-7">
                                            <div class="input-group">
                                                <input type="text" class="form-control" id="sob"
                                                    placeholder="Size of Board" />
                                                <span class="input-group-addon">px</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="font-family" class="col col-sm-5">Font</label>
                                        <div class="col col-sm-7">
                                            <select class="form-control" id="font-family">
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="font-size" class="col col-sm-5">Font Size</label>
                                        <div class="col col-sm-7">
                                            <div class="input-group">
                                                <input type="text" class="form-control" id="font-size"
                                                    placeholder="Font size" />
                                                <span class="input-group-addon">mm</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="color" class="col col-sm-5">Color</label>
                                        <div class="col col-sm-7">
                                            <input type="text" class="form-control" id="color" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="outline-thickness" class="col col-sm-5">Outer Line</label>
                                        <div class="col col-sm-7">
                                            <div class="input-group">
                                                <input type="text" class="form-control" id="outline-thickness"
                                                    placeholder="Outer line" />
                                                <span class="input-group-addon">px</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="outline-color" class="col col-sm-5">Outer Line Color</label>
                                        <div class="col col-sm-7">
                                            <input type="text" class="form-control" id="outline-color" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="innerline-thickness" class="col col-sm-5">Inner Line</label>
                                        <div class="col col-sm-7">
                                            <div class="input-group">
                                                <input type="text" class="form-control" id="innerline-thickness"
                                                    placeholder="Inner line" />
                                                <span class="input-group-addon">px</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="innerline-color" class="col col-sm-5">Inner Line Color</label>
                                        <div class="col col-sm-7">
                                            <input type="text" class="form-control" id="innerline-color" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="bg-color" class="col col-sm-5">Background Color</label>
                                        <div class="col col-sm-7">
                                            <input type="text" class="form-control" id="bg-color" />
                                        </div>
                                    </div>
                                </form>
                            </fieldset>
                        </div>
                    </div>
                </div>
            </div>

        </Modal.Body>
        <Modal.Footer>
            <Button variant="secondary" onClick={handleClose}>
                Close
            </Button>
            <Button variant="primary" onClick={handleClose}>
                Save Changes
            </Button>
        </Modal.Footer>
    </Modal>
</div>
            </body>
        );
    }
}
export default Home;*/