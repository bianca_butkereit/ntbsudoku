import React /*, { Component }*/ from 'react';

import './css/App.css';
import 'bootstrap/dist/css/bootstrap.min.css';

import Button from 'react-bootstrap/Button';
import Collapse from 'react-bootstrap/Collapse';
import * as d3 from "d3";
import html2canvas from "html2canvas";
import jsPDF from "jspdf";

import * as $ from "jquery";
//import './scripts/svgToPdf.js';

import {
    getSudoku,
    getVeryEasySudoku,
    getEasySudoku,
    getMediumSudoku,
    getHardSudoku
  } from "fake-sudoku-puzzle-generator";

//import saveAs from 'file-saver';
var SudokuSolver = require('sudoku-solver-js');
var solver = new SudokuSolver();


var diff = "Sudoku lett"; 
var xml1, xml2, xml3, xml4, xml5, xml6, xml7 = '';
var svg1, svg2, svg3, svg4, svg5, svg6, svg7 ;
var svgResult1, svgResult2, svgResult3, svgResult4, svgResult5, svgResult6, svgResult7;
var mypdf = new jsPDF();
function generateMyBoard(nr, diff_level, isPrint, resNr){
    var nr2 =  1;
    console.log(resNr + "  resNr --------------")
    console.log(diff_level)
    var puzzle;
    var headline = "";
    if(diff_level == 0){
        headline = diff1;
        diff = diff1;
        puzzle = getVeryEasySudoku();
    }else if(diff_level == 1){
        headline = diff2;
        diff = diff2;
        puzzle = getEasySudoku();
    }else if(diff_level == 2){
        headline = diff3;
        diff = diff3;
        puzzle = getMediumSudoku();
    }else if(diff_level == 3){
        headline = diff4;
        diff = diff4;
        puzzle = getHardSudoku();
    }
    

  
  var nr = nr;
    /**** test new script ****/
 // var puzzle = getEasySudoku();
  console.log(puzzle)
  var testSudoku ="";
  var Grid;
  for(var i=0; i < puzzle.length; i++) {
      var line = puzzle[i];
     // console.log(line)
      for (var k=0; k < line.length;k++){
         // console.log(line[k])
          if(line[k] == null){
             // console.log("is null")
              //puzzle[i][k] = 0;
              testSudoku+= ".";
              puzzle[i][k] = "";
          }else{
            testSudoku+=line[k]
          }
        //line[k] = line[k].replace("0", "null");
      }
    
   }
   console.log(testSudoku);
   var results = solver.solve(testSudoku, { result: 'chunks' }) //sudoku.solver.solve(testSudoku);
   console.log(results);
   var puzzleT ;
   setTimeout(function(){ 
       if(results != false){
        //puzzleT = sudoku.conversions.stringToGrid(results);
      
    
      
    console.log( "puzzlet : ")
   console.log(results)
   console.log("puzzle : ")
   console.log(puzzle)

   Grid = gridData(results, puzzle, nr2);

   
  //var results = sudokuSolver.solve_string(testSudoku);
 /* if(results.solved){
      console.log("solved !!!! "+ results);
  }else {
      console.log("not solved" + results.solution)
  }*/

  /*************************/
    
  var isPrint = isPrint;
  if(resNr){nr2=resNr}
  //console.log ("nr: "+nr+"diff_level: "+diff_level+" isprint : "+isPrint+" resNr: "+resNr)
 // SudokuGenerator.generate(3);

  // get difficulty sheets
                // let l_oFirstBoard = SudokuGenerator.generatedBoards[0];

  // get sheet signature (for loading)
 // console.log(l_oFirstBoard.signature);

  // load saved board 
  //let l_oLoadedBoard = SudokuGenerator.loadBoard(l_oFirstBoard.signature);
/*
  // get hard difficulty sheet
  let l_aHardSheet = l_oFirstBoard.getSheet(2);

  // get medium difficulty sheet
  let l_aMediumSheet = l_oFirstBoard.getSheet(1);
  
  // get easy difficulty sheet
  let l_aEasySheet = l_oFirstBoard.getSheet(0);
*/  
  // pretty print solution to console
  //l_oFirstBoard.prettyPrint();
  
  //var testing = $("#root").html(l_oFirstBoard)
  // pretty print sheet
  //l_oFirstBoard.prettyPrint(l_aEasySheet);

  
  
  //var Grid = gridData(l_oFirstBoard, mySudoku, nr2);
 // console.log(Grid)
  var node = <div></div>;
  //$("#myResults"+nr2+" .grid"+nr+" .svgC").html("");sol1
  $("#myResults"+nr2+" .grid"+nr+" .hl").html( headline ).attr("style","color:"+hlcolor);
  $("#myResults"+nr2+" .sol"+nr+" .hl").html( headline ).attr("style","color:"+hlcolor+"; font-size:1.3rem;margin-left:193px;margin-bottom:-21px;margin-top:15px");//<h6 style='color:"+hlcolor+";text-align:right;margin-right:253px;'>
  $("#myResults"+nr2+" .sol"+nr+" .svgC").html("");
 
  $("#myResults"+nr2+" .grid"+nr+" .svgC").html("");
  var grid = d3.select("#myResults"+nr2+" .grid"+nr+" .svgC")
    .append("svg")
    .attr("id", "svgC1")
    .attr("width","510px")
    .attr("height","510px");

    var row = grid.selectAll(".row")
    .data(Grid)
    .enter().append("g")
    .attr("class", "row")
    .style("stroke-width",1);  

    var column = row.selectAll(".square")
    .data(function(d) { return d; })
    .enter().append("rect")
    .attr("class","square")
    .attr("x", function(d) { return d.x + 10; })
    .attr("y", function(d) { return d.y + 10; })
    .attr("width", function(d) { return d.width; })
    .attr("height", function(d) { return d.height; })
    //.style("background-color", "#fff")
    .style("stroke-width", 1)
    .style("stroke", thincolor)
    .style("fill",function(d) { return d.bgColor; });
    
    var text = row.selectAll(".text")
      .data(function(d){return d;})
      .enter().append("text")
      .attr("x", function(d) { return d.x +23; })
      .attr("y", function(d) { return d.y + 48; })
      .attr("width", function(d) { return d.width; })
      .attr("height", function(d) { return d.height; })
      .style("font-size","36px")
      .style("font-family", "arial")
      .style("font-weight" , "bold")
      .style("fill", fontcolor)
      .text(function(d){return d.value;})
    

      for(var i = 0; i < 9 / 3; i++) {
        for(var j = 0; j < 9 / 3; j++) {
            grid.append("rect")
                .attr("height", 450/3)
                .attr("width", 450/3)
                .attr("transform", "translate("+ (i * 450/3   +10) + "," + (j * 450/3  +10) + ")")
                .attr("fill-opacity", "0")
                .attr("pointer-events", "none")
                .classed("box", true)
                .style("stroke-width", 4)
                
                .style("stroke",thickcolor);
        }
      }
      

      //LOESUNG hl-result
     // $("#myResults"+nr2+" .sol"+nr+"").html("<h6 style='color:"+hlcolor+";text-align:right;margin-right:253px;'>Løsning</h6>").attr("style","")
      $("#myResults"+nr2+" .hl-result").html("<h6 style='color:"+hlcolor+";text-align:left;margin-left:200px;'>Løsning</h6>").attr("style","")
    var grid2 = d3.select("#myResults"+nr2+" .sol"+nr +" .svgC")
    .append("svg")
    .attr("width","510px")
    .attr("height","510px")
    .attr("viewBox", "-85 -400 900 900");

    var row2 = grid2.selectAll(".row")
    .data(Grid)
    .enter().append("g")
    .attr("class", "row")
    .style("stroke-width",1);  

    var column2 = row2.selectAll(".square")
    .data(function(d) { return d; })
    .enter().append("rect")
    .attr("class","square")
    .attr("x", function(d) { return d.x + 10; })
    .attr("y", function(d) { return d.y + 10; })
    .attr("width", function(d) { return d.width; })
    .attr("height", function(d) { return d.height; })
    .style("fill", "#fff")

    .style("stroke-width", 1)
    .style("stroke", thincolor)
    .style("fill",function(d) { return d.bgColor; });
 
    
    var text2 = row2.selectAll(".text")
      .data(function(d){return d;})
      .enter().append("text")
      .attr("x", function(d) { return d.x +26; })
      .attr("y", function(d) { return d.y + 44; })
      .attr("width", function(d) { return d.width; })
      .attr("height", function(d) { return d.height; })
      .style("font-size","28px")
      .style("font-family", "arial")
      .style("font-weight" , "bold")
      //.style("fill", fontcolor)
      .style("fill",  fontcolor)
      .text(function(d){return d.value2;})


      for(var k = 0; k < 9 / 3; k++) {
        for(var l = 0; l < 9 / 3; l++) {
            grid2.append("rect")
                .attr("height", 450/3)
                .attr("width", 450/3)
                .attr("transform", "translate("+ (k * 450/3   +10) + "," + (l * 450/3  +10) + ")")
                .attr("fill-opacity", "0")
                .attr("pointer-events", "none")
                .classed("box", true)
                .style("stroke-width", 4)
                .style("stroke", thickcolor);
        }
      }
      grid2
        .attr("transform"," rotate(180 0 0)")
        
   var blub = nr2 +1;
    for(var m =blub; m <= 7;m++){
        $("#myResults"+m+"  .hl").html( "" );
        $("#myResults"+m+" .hl-result").html("");
    }
   svg1 = grid;
   svgResult1 = grid2;
    return grid;
}
},300);
   
}
var today=new Date();
var dd = String(today. getDate()). padStart(2, '0');
var mm = String(today. getMonth() + 1). padStart(2, '0');
var yyyy = today. getFullYear();
today = dd + '_' + mm + '_' + yyyy;
var fontcolor = "#000000";
var bgcolor = "#ffffff";
var bgcolor2 = "#ffffff";
var thickcolor = "#000000";
var thincolor = "#000000";
var hlcolor = "#000000";
var theme = "none";
var diff1 = "VELDIG LETT";
var diff2 = "LETT";
var diff3 = "MIDDELS";
var diff4 = "VANSKELIG";
var sReps = 1;


function handleSubmit(){
 
  $(".hl").html("");
  $(".svgC").html("");
  $(".hl-result").html("");

  //setOpen(!open);
  const input = document.getElementById('myForm');
  //var form = $('#myForm2').serialize();
  var form2 =$('#myForm2').serializeArray();
  var counter = 1;
  console.log("FOOOOOOOOOOOOOOOOOOORM")
  console.log(form2)
  $.each(form2, function(k,v){
    

    if(v.name==="sudokusReps"){
        if (v.value > 1){
            sReps = v.value;
        }else {
            sReps = v.value;
        }

    }
    if( v.name==="fontcolor" || 
        v.name==="bgcolor" || 
        v.name==="bgcolor2" ||
        v.name==="thickcolor" ||
        v.name==="thincolor" ||
        v.name==="hlcolor"){
            var tempstr = v.value;
            if(tempstr.charAt(0)!=='#'){
                v.value = '#'+v.value;
            }

    }

    if (v.name==="fontcolor") fontcolor = v.value;
    if (v.name==="theme") theme = v.value;
    if (v.name==="bgcolor") bgcolor = v.value;
    if (v.name==="bgcolor2") bgcolor2 = v.value;
    if (v.name==="thickcolor") thickcolor = v.value;
    if (v.name==="thincolor") thincolor = v.value;
    if (v.name==="hlcolor") hlcolor = v.value;
    if (v.name==="diff1") diff1 = v.value; 
    if (v.name==="diff2") diff2 = v.value;
    if (v.name==="diff3") diff3 = v.value;
    if (v.name==="diff4") diff4 = v.value;
   
   // if (v.name==="diff_level") {
       
      
     
      counter++;
   // }
  })
  if(sReps > 1){
    makePdf(sReps, form2)   
}else {
   // generateMyBoard(counter, v.value) 
   makePdf(1, form2)   
}
}
function makePdf(days, form2){
    //console.log("makePdf")
    var pdfSets = days;
    var counterI = 1;
    // for each day do x amount of sodukos and print to pdf
    for(var n = 0; n <= pdfSets;n++){

    
        $.each(form2, function(k,v){
           // console.log(v.name+" :"+v.value)
        
            if (v.name==="diff_level") {
                {
                    console.log(k,v)
                    console.log(n)
                    var board =  generateMyBoard(counterI, v.value, true, n) ;
                  //  var doc = new jsPDF({
                  //      format:"a4"
                  //  })
                   // console.log(board)
                    //doc.addSvgAsImage(board, 10, 10,  200,200);
                  // doc.save();
                }
            
            counterI++;
            }
        })
      counterI = 1;
     // console.log($("#diff_level_0:checked").length);
      var tempcounter = $("#diff_level_0:checked").length + $("#diff_level_1:checked").length + $("#diff_level_2:checked").length;
        if( tempcounter == 0){
            console.log(n)
            generateMyBoard(counterI, 0, true, n) ;
        }
    }
    

}
function gridData(dataF,dataH, nr2){
    console.log(nr2 + "  nr 2 start")
  console.log("griddata")
  console.log(dataH )
  console.log(dataF )
 // console.log(dataF.board) // data fuer loesung
 // console.log(dataH)        // data fuer frei
  var counterCol = 0;
  var counterRow = 0
 // var str = dataH.join("\n"); console.log(" my string is : "+str.length)
 // var dataH = str;
  var data = new Array();
    var xpos = 1; //starting xpos and ypos at 1 so the stroke will show when we make the grid below
    var ypos = 1;
    var width = 50;
    var height = 50;
    //var theme = "node";
    // iterate for rows 
    var xml='<?xml version = "1.0" encoding = "UTF-8" ?> \n'
    +'<crossword-compiler xmlns="http://crossword.info/xml/crossword-compiler"> \n'
    +    '<rectangular-puzzle xmlns="http://crossword.info/xml/rectangular-puzzle" alphabet="123456789"> \n'
    +        '<metadata> \n'
    +            '<title>'+ diff +'</title> \n'
    +            '<creator>(c) NTB</creator> \n'
    +            '<copyright></copyright> \n'
    +            '<description></description> \n'
    +        '</metadata> \n'
    +        '<sudoku box-width="3" box-height="3"> \n'
    +            '<grid width="9" height="9"> \n'
    +                '<grid-look thick-border="true" numbering-scheme="none" cell-size-in-pixels="34" clue-square-divider-width="0.7" /> \n';
    var topB;
    var leftB;
   // console.log("bgcolor :" + bgcolor)
    for (var row = 0; row < 9; row++) {
        data.push( new Array() );

        // iterate for cells/columns inside rows
        for (var column = 0; column < 9; column++) {
           // console.log("row :" + counterRow)
           // console.log( "col : "+counterCol)
           
           var bgColor = bgcolor2;
           if(theme==="chess" && 
                ((row % 2===0 && column % 2===0) ||
                row % 2!==0 && column % 2!==0)
            ){
               bgColor = bgcolor;     
           }else if (theme==="cross" &&
                ((row!==1 && row!==4 && row!==7) &&
                    (column!==1 && column!==4 && column!==7)
                )
           
           ){
                bgColor = bgcolor; 
           }else if (theme==="cross" &&
                ((row===1 || row===4|| row===7) &&
                    (column===1 || column===4 || column===7)
           )){
                bgColor = bgcolor; 
           }else if(theme==="bigCross" &&
                    ((row!==3 && row!==4 && row!==5) && (
                        column!==3 && column!==4 && column!==5
                    ))){
                        bgColor = bgcolor; 
           }else if(theme==="bigCross" &&
                    ((row===3 || row===4 || row===5) && 
                        (column===3 || column===4 || column===5)
            )){
                        bgColor = bgcolor; 
            } else {
               bgColor = bgcolor2;
           }
           //----     
           // xml
            var hasHint = false;
            if( dataH[counterRow][counterCol]!==""){
                hasHint = true;
            }else {
                hasHint = false;
            }
            if (column === 3 || column === 6){topB = true;}else {topB = false;} // top lines for xml
            if (row === 3 || row === 6){leftB = true;}else {leftB = false;} // side lines for xml
          // xml += '<cell x="'+ (row+1) +'" y="'+ (column+1) +'" solution="'+dataF.board[counterRow][counterCol]+'" hint="'+ hasHint+'" top-bar="'+ topB +'"  left-bar="'+ leftB +'" /> \n';
           //const cell = (props) => React.createElement("cell", props);
           xml += '<cell x="'+ (row+1) +'" y="'+ (column+1) +'" solution="'+dataF[counterRow][counterCol]+'" hint="'+ hasHint+'" top-bar="'+ topB +'"  left-bar="'+ leftB +'" /> \n';
           
           //----
         //  console.log( dataH[counterRow][counterCol])
            data[row].push({
                x: xpos,
                y: ypos,
                width: width ,
                height: height,
                value: dataH[counterRow][counterCol], 
                //value2: dataF.board[counterRow][counterCol],
                value2: dataF[counterRow][counterCol],
                bgColor: bgColor,
                hint:hasHint
            })
            //console.log(" my cell value : " +dataH[counterRow][counterCol])
            // increment the x position. I.e. move it over by 50 (width variable)
            xpos += width;
            counterCol++;
            
        }
        // reset the x position after a row is complete
        xpos = 1;
        // increment the y position for the next row. Move it down 50 (height variable)
        ypos += height; 
        counterRow++;
        counterCol = 0;
       
        
    }
    xml += '</grid> \n</sudoku> \n</rectangular-puzzle> \n</crossword-compiler>';
     //console.log(xml);
    // var parser = new DOMParser();
    xmlDoc = xml;//parser.parseFromString(xml, "text/xml");
    console.log(nr2 + "  nr2")
    //console.log(xmlDoc)
    if(nr2===1) xml1=xmlDoc;
    else if(nr2===2) xml2=xmlDoc;
    else if(nr2===3) xml3=xmlDoc;
    else if(nr2===4) xml4=xmlDoc;
    else if(nr2===5) xml5=xmlDoc;
    else if(nr2===6) xml6=xmlDoc;
    else if(nr2===7) xml7=xmlDoc;
    return data;
}
var  xmlDoc;
function handleXMLMass(){
    for(var i = 1;i<= sReps; i++){
       var tempXml;
       if(i===1) tempXml=xml1;
       else if(i===2) tempXml=xml2;
       else if(i===3) tempXml=xml3;
       else if(i===4) tempXml=xml4;
       else if(i===5) tempXml=xml5;
       else if(i===6) tempXml=xml6;
       else if(i===7) tempXml=xml7;
       var blob = new Blob([tempXml], {type:"application/xml"});
       var Filesaver = require('file-saver');
       Filesaver.saveAs(blob, "-"+i+".xml")
    }
    
}
function handleXML(){
   // console.log(handleXML)
   // console.log(xmlDoc)
    //var doc = new DOMParser().parseFromString(xmlDoc);
    //var blob = new Blob([xmlDoc], {type:"application/xml"});
   // var Filesaver = require('file-saver');
   // Filesaver.saveAs(blob, "test.xml")
   // doc.saveAs("sudoku.xml","application/xml");
    
   // console.log(doc);


}
var doc;
function handlePrint2(){
  /*  var pdf = new jsPDF();
    var svg = $("#svgC1");
    console.log(svg1);
    var svg2 = new XMLSerializer().serializeToString(svg[0]);
    //console.log(svg2)
    pdf.setDrawColor(255, 0, 0)
    //pdf.line(34, 30, 100, 30)
    pdf.addSvg(svg2, 1, 1, 400, 400)

    pdf.save( 'pdf.pdf');*/
    ///////
   /* var doc = new jsPDF('l', 'px', 'a4');
    doc.setDrawColor(0, 0, 0,1);
      var c = doc.canvas;
            c.width = 1000;
            c.height = 500;
            var svg = $("#svgC1");
            var svg2 = new XMLSerializer().serializeToString(svg[0]);
            var ctx = c.getContext('2d');
            ctx.ignoreClearRect = true;
            ctx.fillStyle = '#ffffff';
            ctx.fillRect(0, 0, 700, 1000);

            //load a svg snippet in the canvas with id = 'drawingArea'
            canvg(c, svg2, {
                ignoreMouse: true,
                ignoreAnimation: true,
                ignoreDimensions: true
            });
            doc.save('Test.pdf');*/
}

function handlePrint(){
    $(".mySpinner").attr("style","display:block;");
  //console.log("handleprint")
  //const input  = document.getElementById('myResults');
  const input1 = document.getElementById('side1');
  const input2 = document.getElementById('side2');
  const input3 = document.getElementById('side3');
  const input4 = document.getElementById('side4');
  const input5 = document.getElementById('side5');
  const input6 = document.getElementById('side6');
  const input7 = document.getElementById('side7');

    //var doc = new jsPDF("p","mm");
     doc = new jsPDF({
        format:"a4"
    })
    doc.page = 1;
    var imgWidth = 120; 
    //var pageHeight = 295;  
    var d1 = $.Deferred();
    var d2 = $.Deferred();
    var d3 = $.Deferred();
    var d4 = $.Deferred();
    var d5 = $.Deferred();
    var d6 = $.Deferred();
    var d7 = $.Deferred();
    

   // var imgHeight = imgData1.height * imgWidth / imgData1.width;
    //var heightLeft = imgHeight;
    html2canvas(input1,{
        scrollX:0,
        scrollY:0,
        scale:5,
        quality:4/*,
       // scale:5*/
    })
    .then((canvas1) => {
        makeHeader();
        footer();
        var imgData1 = canvas1.toDataURL('image/jpeg');
        var imgHeight1 = (canvas1.height * imgWidth) / canvas1.width;
        
        if(imgHeight1 > 0){
            doc.addImage(imgData1,'JPEG', 10, 10,  imgWidth,imgHeight1 );
            
            d1.resolve();
        }else {/*d1.resolve();*/}
        
        
    });
   
    if (sReps > 1 ){
    html2canvas(input2,{
            scrollX:0,
            scrollY:0,
            quality:4,
            scale:5
    })
    .then((canvas2) => {
              var imgData2 = canvas2.toDataURL('image/jpeg');
              var imgHeight2 = canvas2.height * imgWidth / canvas2.width;
            
              if(imgHeight2 > 0) {
                $.when (d1 ).done(function(){
                doc.addPage();
                makeHeader();
                footer();
                doc.addImage(imgData2,'JPEG', 10, 10, imgWidth,imgHeight2 );
                d2.resolve();
               })
                
            }
          
    });
}else {
    d2.resolve();
    d3.resolve();
    d4.resolve();
    d5.resolve();
    d6.resolve();
    d7.resolve();
}
    if (sReps > 2){

    
     html2canvas(input3,{
                scrollX:0,
                scrollY:0,
                quality:4,
                scale:5
    })
    .then((canvas3) => {
                  var imgData3 = canvas3.toDataURL('image/jpeg');
                  var imgHeight3 = canvas3.height * imgWidth / canvas3.width;
                
                  if(imgHeight3 > 0){
                    $.when (d2).done(function(){
                    doc.addPage();makeHeader();footer();
                       doc.addImage(imgData3,'JPEG', 10, 10, imgWidth,imgHeight3 );
                       d3.resolve();
                    })
                  }
                
    });
}else {
    d3.resolve();
    d4.resolve();
    d5.resolve();
    d6.resolve();
    d7.resolve();
}
    //make if sReps > 3 ,>4 ... then canvas4 5 ... else d4.resolfe()
    if (sReps > 3){
     
        html2canvas(input4,{
            scrollX:0,
            scrollY:0,
            quality:4,
            scale:5
        })
            .then((canvas4) => {
                    var imgData4 = canvas4.toDataURL('image/jpeg');
                    var imgHeight4 = canvas4.height * imgWidth / canvas4.width;
                   
                    if(imgHeight4 > 0){
                       
                        $.when (d3).done(function(){
                           
                            doc.addPage();makeHeader();footer();
                            doc.addImage(imgData4,'JPEG', 10, 10, imgWidth,imgHeight4 );
                            d4.resolve();
                        })
                    }
                   
        });

    }else {
        d4.resolve();
        d5.resolve();
        d6.resolve();
        d7.resolve();
    }
    if (sReps > 4){
        html2canvas(input5,{
            scrollX:0,
            scrollY:0,
            quality:4,
            scale:5
        })
            .then((canvas5) => {
                    var imgData5 = canvas5.toDataURL('image/jpeg');
                    var imgHeight5 = canvas5.height * imgWidth / canvas5.width;
                   
                    if(imgHeight5 > 0){
                        $.when (d4 ).done(function(){
                        doc.addPage();makeHeader();footer();
                        doc.addImage(imgData5,'JPEG', 10, 10, imgWidth,imgHeight5 );
                        d5.resolve();
                        })
                    }
                   
        });

    }else {
        d5.resolve();
        d6.resolve();
        d7.resolve();
    }
    if (sReps > 5){
        html2canvas(input6,{
            scrollX:0,
            scrollY:0,
            quality:4,
            scale:5
        })
            .then((canvas6) => {
                 var imgData6 = canvas6.toDataURL('image/jpeg');
                    var imgHeight6 = canvas6.height * imgWidth / canvas6.width;
                    
                    if(imgHeight6 > 0){
                        $.when (d5).done(function(){
                        doc.addPage();makeHeader();footer();
                        doc.addImage(imgData6,'JPEG', 10, 10, imgWidth,imgHeight6 );
                        d6.resolve();
                        })
                    }
                  
        });

     }else {
        d6.resolve();
        d7.resolve();
    }
    if (sReps > 6){
        html2canvas(input7,{
            scrollX:0,
            scrollY:0,
            quality:4,
            scale:5
        })
            .then((canvas7) => {
                    var imgData7 = canvas7.toDataURL('image/jpeg');
                    var imgHeight7 = canvas7.height * imgWidth / canvas7.width;
                    // console.log("image height 3 :" +imgHeight3)
                    if(imgHeight7 > 0){
                        $.when (d6).done(function(){
                        doc.addPage();makeHeader();footer();
                        doc.addImage(imgData7,'JPEG', 10, 10, imgWidth,imgHeight7 );
                        d7.resolve();
                        })
                    }
                   // d7.resolve();  
        });

    }else {
        d7.resolve();
     }
   
    $.when (d1, d2, d3, d4, d5, d6, d7 ).done(function(){
        
        doc.save( 'pdf'+today+'.pdf');
        $(".mySpinner").attr("style","display:none !important;");
        //console.log("saving")
    });
    
   var page =  doc.internal.getNumberOfPages();
   //console.log(page + " PAAAAAAAAAAAAAGE")
}
function makeHeader(){
    doc.text(135,10,'generert den ' +dd+'.'+mm+'.'+yyyy);
}
function footer(){
    doc.text(150,285,'side ' +doc.page+ ' av '+ sReps);
    doc.page++;
}
function refreshSudoku(){
    handleSubmit();
}
function App() {
  //console.log("APP")
  const [show, setShow] = React.useState(false);
  const [open, setOpen] = React.useState(false);
  const handleClose = () => setShow(false);
  //const handleShow = () => setShow(true);
  //const ids = ['myResults'];

   generateMyBoard(1,'0',false,0);


  //console.log(grid._groups[0][0])
 // console.log(grid)
  //var grid2 = <h2></h2>;
 // generateMyBoard(1,'0');
  return (

    <div className="App">
     
     <div className="print-main page">
     <Button id="refresh-S" title="hente nytt spill" onClick={refreshSudoku} className="btn btn-info" value="Stampa">
            <i className="fa  fa-refresh" aria-hidden="true"></i>
        </Button>
        <Button id="pdf-download" title="pdf print" onClick={handlePrint} className="btn btn-info" value="Stampa">
            <i className="fa  fa-file-pdf-o"></i>
        </Button>
        {/* 
        <Button id="xml-download" title="xml print" onClick={handleXML} className="btn btn-info" value="Stampa">
            <i className="fa  fa-file-code-o"></i>
        </Button>
        */}
        <Button id="xml-download2" title="xml print" onClick={handleXMLMass} className="btn btn-primary btn2" value="Stampa">
            <i className="fa  fa-file-code-o"></i>
        </Button>
        <Button 
          id="btn-settings"
          onClick={() => setOpen(!open) }
          aria-controls="collapseForm"
          title="innstillinger"
          className="btn btn-info"
          aria-expanded={open}>
            <i className="fa fa-cog"></i>
        </Button>

{/*  

        <Button id="settings" type="button" className="btn btn-info" onClick={handleShow}>
            <i className="fa fa-cog"></i>
        </Button>
*/}
    </div>
   {/* <div id="grid4">{grid2}</div>*/}
       <div>
        
          <Collapse in={open} >
            <div id="collapseForm">
              <form id="myForm2" className="form-horizontal" /*onSubmit={handleSubmit}*/>
              
                <div className="form-group">
                   <h6>Tema og farger</h6> 
                </div>
                <div className="form-group">
                  <label /*for="nod"*/ className="col col-sm-6">Tema</label>
                  <div className="col col-sm-7">
                    <select className="form-control" id="theme" name="theme">
                      <option value="none">Ingen</option>
                      <option value="cross">Kryss</option>
                      <option value="bigCross">Stort kryss</option>
                      <option value="chess">Sjakk</option>
                      
                    </select>
                  </div>
                </div>
              <div className="form-group">
                    <label  className="col col-sm-6">Farge tall:  </label>
                  {/*  <input type="color" id="fontcolor" name="fontcolor" defaultValue="#000000" ></input>*/} 
                    <input type="text" name="fontcolor" className="colorpicker-popup" id="colorpicker-popup" defaultValue="000000"></input>
                </div>
                <div className="form-group">
                    <label  className="col col-sm-6">Farge bakgrunn:     </label>
                    {/* <input type="color" id="bgcolor" name="bgcolor" defaultValue="#fdffff" ></input>*/} 
                    <input type="text" name="bgcolor" className="colorpicker-popup" id="colorpicker-popup" defaultValue="ffffff"></input>
                </div>

                <div className="form-group">
                    <label  className="col col-sm-6">Farge tykk strek:</label>
                  {/*  <input type="color" id="thickcolor" name="thickcolor" defaultValue="#000000" ></input>*/} 
                    <input type="text" name="thickcolor" className="colorpicker-popup" id="colorpicker-popup" defaultValue="000000"></input>
                </div>
                <div className="form-group">
                    <label  className="col col-sm-6">Farge tynn strek:</label>
                   {/* <input type="color" id="thincolor" name="thincolor" defaultValue="#000000" ></input>*/} 
                    <input type="text" name="thincolor" className="colorpicker-popup" id="colorpicker-popup" defaultValue="000000"></input>
                </div>
                <div className="form-group">
                    <label className="col col-sm-6" >Farge tittel:</label>
                   {/* <input type="color" id="hlcolor" name="hlcolor" defaultValue="#000000" ></input>*/} 
                    <input type="text" name="hlcolor" className="colorpicker-popup" id="colorpicker-popup" defaultValue="000000"></input>
                </div>
                <div className="form-group">
                   <h6>Nivå og overskrifter</h6> 
                </div>
                <div className="form-group">
                  <label /*for="nod"*/ className="col col-sm-6">Antall</label>
                  <div className="col col-sm-7">
                    <select className="form-control" id="nor" name="sudokusReps">
                      <option value="1">1</option>
                      <option value="2">2</option>
                      <option value="3">3</option>
                      <option value="4">4</option>
                      <option value="5">5</option>
                      <option value="6">6</option>
                      <option value="7">7</option>
                     
                      
                    </select>
                  </div>
                </div>

               
                <div className="form-group">
                    
                    <label className="col col-sm-6" >Overskrift lett: </label>
                    <input type="text" id="diff1" name="diff1" defaultValue="veldig lett" ></input>
                </div>
                <div className="form-group">
                    <label className="col col-sm-6" >Overskrift lett: </label>
                    <input type="text" id="diff2" name="diff2" defaultValue="lett" ></input>
                </div>
                <div className="form-group">
                    <label className="col col-sm-6" >Overskrift middels: </label>
                    <input type="text" id="diff3" name="diff3" defaultValue="middels" ></input>
                </div>
                <div className="form-group">
                    <label className="col col-sm-6" >Overskrift vanskelig: </label>
                    <input type="text" id="diff4" name="diff4" defaultValue="VANSKELIG" ></input>
                </div>
                
                <div className="form-group">
                  <label /*for="print-dif"*/ className="col col-sm-6">Nivåer som skal brukes</label>
                  <div className="col col-sm-7">
                  
                    <input type="checkbox" className="diff_level" name="diff_level" id="diff_level_0"
                      value="0" />&nbsp; super LETT<br />
                    <input type="checkbox" className="diff_level" name="diff_level" id="diff_level_1"
                      value="1" />&nbsp;lett<br />
                    <input type="checkbox" className="diff_level" name="diff_level" id="diff_level_2"
                      value="2" />&nbsp;middels<br />
                     <input type="checkbox" className="diff_level" name="diff_level" id="diff_level_3"
                      value="3" />&nbsp;VANSKELIG<br />
                    
                    
                  </div>
                </div>
             
                
                <div className="form-group">
                <Button onClick={handleSubmit } onMouseUp={() => setOpen(!open)} >Lagre</Button>
                </div>
               
              </form>
            </div>
          </Collapse>
       </div>
    
   
</div>

  )
  
}
export default App;
